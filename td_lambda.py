import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error


NUM_STATES = 5
INITIAL_STATE = 2
LEFT_TERMINAL_STATE = -1
RIGHT_TERMINAL_STATE = NUM_STATES

NUM_TRAINING_SETS = 100
SEQUENCE_LEN = 10


def training_sequences():
    sequences = []
    for _ in range(SEQUENCE_LEN):
        location = INITIAL_STATE
        state_sequence = [location]
        while location != LEFT_TERMINAL_STATE and \
            location != RIGHT_TERMINAL_STATE:
            move = np.random.choice([-1, 1])
            location += move
            state_sequence.append(location)
        sequences.append(state_sequence)

    return sequences


def update_weights_with_sequence(sequence, w_t, l, a):
    # Create initial state vector
    x_t = np.zeros((NUM_STATES, ))
    x_t[sequence[0]] = 1

    e_t = x_t
    P_t = np.asscalar(np.matmul(w_t.T, x_t))
    d_w_t = np.zeros((NUM_STATES, ))

    # Ignore the starting and terminating states
    for location in sequence[1:-1]:
        # Create state vector
        x_t_1 = np.zeros((NUM_STATES, ))
        x_t_1[location] = 1
        P_t_1 = np.asscalar(np.matmul(w_t.T, x_t_1))
        d_w_t += a * (P_t_1 - P_t) * e_t
        e_t = x_t_1 + l * e_t
        P_t = P_t_1

    # Z is the last observation and the outcome of the sequence
    z = 1 if sequence[-1] == RIGHT_TERMINAL_STATE else 0
    d_w_t += a * (z - P_t) * e_t

    return d_w_t


def experiment2_training(sequences, l, a):
    w_t = np.full((NUM_STATES, ), 0.5)
    for s in sequences:
        w_t += update_weights_with_sequence(s, w_t, l, a)

    return w_t


def experiment1_training(sequences, w_t, l, a):
    sum_d_w_t = np.zeros((NUM_STATES, ))
    for s in sequences:
        sum_d_w_t += update_weights_with_sequence(s, w_t, l, a)

    return w_t + sum_d_w_t


def experiment_1_per_lambda():
    plt.close()
    lambdas = np.arange(0, 1.1, 0.1)
    alphas = np.arange(0.01, 0.03, 0.0009)
    ideal_prediction = np.array([1./6, 1./3, 1./2, 2./3, 5./6])

    rmse_lambdas = []
    for l in lambdas:
        rmse_alphas = []
        for a in alphas:
            rmses = []
            for train_num in range(NUM_TRAINING_SETS):
                # Hardy-Ramanajan for seeding
                np.random.seed(1729 * train_num)
                sequences = training_sequences()

                w_t_1 = np.full((NUM_STATES, ), 0.5)
                for _ in range(1000):
                    w_t = np.copy(w_t_1)
                    w_t_1 = experiment1_training(sequences, w_t_1, l, a)
                    if np.sqrt(mean_squared_error(w_t_1, w_t)) < 0.005:
                        break
                rmses.append(np.sqrt(mean_squared_error(w_t_1, ideal_prediction)))
            rmse_alphas.append(np.mean(rmses))
        rmse_lambdas.append(np.min(rmse_alphas))

    plt.plot(lambdas, rmse_lambdas, "-o")
    plt.xlabel(r"$\lambda$")
    plt.ylabel(r"Error Using Best $\alpha$")
    plt.text(lambdas[-1] - 0.2, rmse_lambdas[-1], 'Widrow-Hoff')
    plt.savefig("figure_3.png", bbox_inches="tight")


def experiment_2_per_alpha():
    plt.close()
    lambdas = [0.0, 0.3, 0.8, 1.0]
    alpha_list = np.arange(0.001, 0.65, 0.05)
    ideal_prediction = np.array([1./6, 1./3, 1./2, 2./3, 5./6])

    rmse_lambda_alphas = []
    for l in lambdas:
        rmse_alphas = []
        for a in alpha_list:
            rmses = []
            for train_num in range(NUM_TRAINING_SETS):
                # Hardy-Ramanajan for seeding
                np.random.seed(1729 * train_num)
                w_t = experiment2_training(training_sequences(), l, a)
                rmses.append(np.sqrt(mean_squared_error(w_t, ideal_prediction)))
            rmse_alphas.append(np.mean(rmses))
        rmse_lambda_alphas.append(rmse_alphas)

    for l, lambda_rmse in zip(lambdas, rmse_lambda_alphas):
        plt.plot(alpha_list, lambda_rmse, "-o")

        idx = len(lambda_rmse) - 1
        for i, error in enumerate(lambda_rmse):
            if 0.6 <= error <= 0.8:
                idx = i
                break

        x = alpha_list[idx] + 0.04
        y = lambda_rmse[idx]
        if l == 1.0:
            x -= 0.3
            plt.text(x, y, r"$\lambda = $ {0:.1f} (Widrow-Hoff)".format(l))
        elif l == 0.0:
            x -= 0.02
            plt.text(x, y, r"$\lambda = $ {0:.1f}".format(l))
        else:
            plt.text(x, y, r"$\lambda = $ {0:.1f}".format(l))
    plt.xlabel(r"$\alpha$")
    plt.ylabel("Error")
    plt.ylim([0, 0.8])
    plt.savefig("figure_4.png", bbox_inches="tight")


def experiment_2_per_lambda():
    plt.close()
    lambdas = np.arange(0.0, 1.1, 0.1)
    alphas = np.arange(0.001, 0.65, 0.05)
    ideal_prediction = np.array([1./6, 1./3, 1./2, 2./3, 5./6])

    rmse_lambdas = []
    for l in lambdas:
        rmse_alphas = []
        for a in alphas:
            rmses = []
            for train_num in range(NUM_TRAINING_SETS):
                # Hardy-Ramanajan for seeding
                np.random.seed(1729 * train_num)
                w_t = experiment2_training(training_sequences(), l, a)
                rmses.append(np.sqrt(mean_squared_error(w_t, ideal_prediction)))
            rmse_alphas.append(np.mean(rmses))
        rmse_lambdas.append(np.min(rmse_alphas))

    plt.plot(lambdas, rmse_lambdas, "-o", label='Widrow-Hoff')
    plt.xlabel(r"$\lambda$")
    plt.ylabel(r"Error Using Best $\alpha$")
    plt.text(lambdas[-1] - 0.2, rmse_lambdas[-1], 'Widrow-Hoff')
    plt.savefig("figure_5.png", bbox_inches="tight")


if __name__ == '__main__':
    experiment_1_per_lambda()
    experiment_2_per_alpha()
    experiment_2_per_lambda()
